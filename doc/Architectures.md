

## Chat application main Architectures:

### Extensible Messaging and Presence Protocol (WhatsApp)

**Protocol main characteristics**

Based in XML. Decentralized. It works like SMTP. Clients do not talk directly to one another and anyone can run a server. Every user on the network has a unique XMPP address (is like SMTP where every user has a unique email address) adding a resource identifying one or more clients eg. user@server/client1 user@server/client2.. Messages are sent (pushed) directly from the server to clients using Bidirectional-streams Over Synchronous HTTP (BOSH) protocol.
 
**Limitations**

Is text based and has high network overhead. Binary data must be base64 encoded.

### MTProto Mobile Protocol (Telegram)

**Protocol main characteristics.**

Cloud based with a focus on security and speed.

Centralized. Designed for access to a server API from clients. The protocol is subdivided into three virtually independent components: High-level component (API query language): defines the method whereby API queries and responses are converted to binary messages. Cryptographic (authorization) layer: defines the method by which messages are encrypted prior to being transmitted through the transport protocol. Transport component: defines the method for the client and the server to transmit messages over some other existing network protocol (such as HTTP, HTTPS, WS (plain websockets), WSS (websockets over HTTPS), TCP, UDP).

Every user is identified by a key ID and a session where messages are exchanged.


### Skype:

Based in peer to peer model. User directory is entirely decentralized and distributed among the nodes in the network, which means the network can scale very easily to large sizes without a complex and costly centralized infrastructure. [skype-reverse-engineering](http://www.oklabs.net/skype-reverse-engineering-the-long-journey/)

## Considerations:

For the purpose of scalability, the decentralized architectures are best. We can still use an hybrid approach, creating an internal communication protocol between servers.

For the implementation, we can rely on services like Firebase, AWS AppSync, etc. But this will limit the future possibility to run our own servers or migrate to another cloud services provider.

For the DB one intuitive solution is NoSQL Databases, but this may potentially create integrity problems a regular and scalable DB like Postgres will prevent.




