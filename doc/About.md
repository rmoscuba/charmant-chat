

# About Charmant Chat:

## Server

**Simple Chat server build with Node.JS**

We use `express` and `socket.io` to create a very simple server.

We use a single channel to connect all the clients.

A message event handler `send` is created, which emit the received message to all clients.
 
## Client

**Simple Chat client**

The client is bootstraped with [Create React App](https://github.com/facebook/create-react-app).

We use `styled-components` to create the UI. And `socket.io-client` to connect to the `socket.io` server.

Every component is made using `Function Components`

### Components

* Avatar
  
  Shows the user avatar

* Editor

  Message input editor

* Message

  Message view


## How it works

Use the `useEffect` Hook to connect to the `socket.io` API, and receive the messages from the server, and add it to the Messages list.

We set the `message` value using the `setMessage` `useState` hook; and update the message list using `setMessages` `useState` hook.


### Deploy

The client is deployed to Vercel.com and is running at [Charmant Chat](https://charmant-chat.rmoscuba.vercel.app/).

The server is deployed to Heroku and is running at [Charmant Server](https://charmantserver.herokuapp.com/)

The server repository is located at [Charmant Server Repo](https://gitlab.com/rmoscuba/charmant-chat-server)

The deploy of the server to Vercel.com is ready. But it appears that the `sokect.io` can not be used or is tricy to be used there due to the [Vercel's serverless functions timeout](https://stackoverflow.com/questions/65379821/cant-connect-to-websocket-server-after-pushing-to-vercel).