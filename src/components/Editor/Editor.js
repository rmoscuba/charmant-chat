import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'


const EditorContainer = styled.div`
  width: auto;
  margin-right: -20px;
  vertical-align: bottom;
`

const Input = styled.input`
  border: none;
  padding: 16px 40px 16px 8px;
  width: 100%;
  &:focus {
    outline: 0px !important;
    -webkit-appearance: none;
    box-shadow: none !important
  }
`;

const SendButton = styled.img`
  width: 40px;
  height: 40px;
  cursor: pointer;
  position: absolute;
  padding-top: 5px;
  margin-left: -40px;
`

export const Editor = (props) => {

  return (
  <>
      <EditorContainer>
      <Input 
        placeholder="Message" 
        value={props.message}
        onChange={(e) => props.setMessage(e.target.value)} 
        onKeyPress={e => e.key === 'Enter' ? props.sendMessage(e) : null }
      />
      <SendButton 
        onClick={(e) => props.sendMessage(e)}
        src={require("./Send.svg").default} 
      />
      </EditorContainer>
  </>
  );

}

Editor.propTypes = {
message: PropTypes.string.isRequired,
setMessage: PropTypes.func.isRequired,
sendMessage: PropTypes.func.isRequired
};

export default Editor
