import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const MessageStyled = styled.div`
  padding: 16px 32px;
  border-radius: 30px;
  width: fit-content;
  max-width: 75%;
  position: relative;
  margin-left: 20px;
`

const MyMessage = styled(MessageStyled)`
  background-color: seashell;
  border-top-right-radius: 0;

  &::before {
    content: "";
    position: absolute;
    background: seashell;
    height: 20px;
    width: 20px;
    right: -20px;
    top: 0;
  }
  &::after {
    content: "";
    position: absolute;
    background: lightblue;
    border-top-left-radius: 20px;
    height: 20px;
    width: 20px;
    right: -20px;
    top: 0;
  }
`

const AlienMessage = styled(MessageStyled)`
  background-color: aliceblue;
  border-top-left-radius: 0;

  &::before {
    content: "";
    position: absolute;
    background: aliceblue;
    height: 20px;
    width: 20px;
    left: -20px;
    top: 0;
  }
  &::after {
    content: "";
    position: absolute;
    background: lightblue;
    border-top-right-radius: 20px;
    height: 20px;
    width: 20px;
    left: -20px;
    top: 0;
  }
`

const Message = (props) => {

  return props.isAlien ? (
    <>
      <AlienMessage>{props.message}</AlienMessage>
    </>
  ) : (
    <>
      <MyMessage>{props.message}</MyMessage>
    </>
  );

}

Message.propTypes = {
  message: PropTypes.string.isRequired,
  isAlien: PropTypes.bool.isRequired,
};

export default Message