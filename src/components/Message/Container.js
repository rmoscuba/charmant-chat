import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components';
import { Message } from '..';

const ContainerStyled = styled.div`
  width: 100%;
  padding: 1px 0;
`;

const MyContainer = styled(ContainerStyled)`
`;

const AlienContainer = styled(ContainerStyled)`
  margin-left: 32px;
`;

const Container = (props) => {

    return (
        props.isAlien ? (
            <>
            <MyContainer>
                <Message message={props.message} isAlien={props.isAlien} />
            </MyContainer>
        </>
        ): (
            <>
            <AlienContainer>
                <Message message={props.message} isAlien={props.isAlien} />
            </AlienContainer>
        </>
        )
    )
}

Container.propTypes = {
    message: PropTypes.string.isRequired,
    isAlien: PropTypes.bool.isRequired
}

export default Container
