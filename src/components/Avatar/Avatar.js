import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components';

const Img = styled.img`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  float: right;
`;

const Avatar = (props) => {

    return (
        <>
            <Img src={props.user.avatarURL} alt={props.user.id} />
        </>
    )

}

Avatar.propTypes = {
    user: PropTypes.shape({
        id: PropTypes.string,
        avatarURL: PropTypes.string
    })
}

Avatar.defaultProps = {
    user: { id: 'user', avatarURL: require('./User1.svg').default }
};

export default Avatar
