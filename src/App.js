import React, { useState, useEffect, useRef } from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import { Avatar, Container, Editor } from './components';
import io from "socket.io-client";

const API = process.env.NODE_ENV !== "production" ? 'http://localhost:4000/' : 'https://charmantserver.herokuapp.com/';

const GlobalStyle = createGlobalStyle`
  * {
    font-family: Arial, Helvetica, sans-serif;
    box-sizing: border-box;
    padding: 0;
    margin: 0;
  }

  html, body, #app, #app>div {
    height: 100%
  }
  
  body {
    min-height: 100vh;
    min-height: -webkit-fill-available;
  }
  
  html {
    height: -webkit-fill-available;
  }

  body {
    background-color: gray;
  }
`;

const MainContainer = styled.div`
  width: 100%;
  max-width: 500px;
  margin-right: auto;
  margin-left: auto;
  padding: 0 20px 20px 0;
  height: 100vh;
  overflow: hidden;
  background-color: lightblue;
`;

const Messages = styled.div`
  height: calc(100% - 26px);
  overflow-y: scroll;
  overflow-x: hidden;
  &::-webkit-scrollbar {
    display: none;
  }
`

const current = Math.floor(Math.random() * 10000);

const user1 = { 
  id: "current", 
  avatarURL: require("./components/Avatar/User1.svg").default 
};

const user2 = { 
  id: "alien", 
  avatarURL: require("./components/Avatar/User2.svg").default 
};

let socket;

function App() {

  const [message, setMessage] = useState('');
  const [message_items, setMessages] = useState([]);

  useEffect(() => {

    socket = io(API);

    socket.on('message', resp => {
      console.log(`message received`);
      const item = {
        message: resp.message,
        user: resp.user !== current ? user1 : user2,
        isAlien: resp.user !== current
      }
      setMessages(msgs => [ ...msgs, item ])
    });
}, []);

  const prop = {
    message: message,
    setMessage: setMessage,
    sendMessage: (event) => {
      event.preventDefault();
      if(prop.message) {
        socket.emit('send', prop.message, current);
        setMessage('')
      }
    }
  }

  const messagesEndRef = useRef(null)

  useEffect(() => {
    messagesEndRef.current?.scrollIntoView({block: "nearest", behavior: "smooth"})
  }, [message_items]);

  return (
    <>
      <GlobalStyle />
      <MainContainer>
        <Messages>

          {message_items.map(item => 
          <>
            <Avatar user={item.user}></Avatar>
            <Container message={item.message} isAlien={item.isAlien} />
          </>
          )}

          <div ref={messagesEndRef} style={{'margin-top': '2px'}} />
        </Messages>

        <Editor
          message={prop.message}
          setMessage={prop.setMessage} 
          sendMessage={prop.sendMessage} 
        />
      </MainContainer>
    </>
  );
}

export default App;
